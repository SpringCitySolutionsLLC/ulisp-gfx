; circle-filled.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a filled circle

; Unfilled circle calculates the points along an eighth of a circle
; and then reflects it eight times.  Filling a circle uses the same
; point calculations but instead of plotting eight reflected points,
; there are four vertical lines.

(defun gfx:circle-filled (center-x center-y radius red green blue)
  (let ((f (- 1 radius))
        (ddF-x 1)
        (ddF-y (* -2 radius))
        (delta-x 0)
        (delta-y radius)
        )

    (gfx:line-vertical center-x (- center-y radius) (+ center-y radius) red green blue)
    (display:pixel (+ center-x radius) center-y red green blue)
    (display:pixel (- center-x radius) center-y red green blue)

    (loop

      (unless (< delta-x delta-y) (return))

      (cond ((>= f 0)
             (setq delta-y (- delta-y 1))
             (setq ddF-y (+ ddF-y 2))
             (setq f (+ f ddF-y)))
            )

      (setq delta-x (+ delta-x 1))
      (setq ddF-x (+ ddF-x 2))
      (setq f (+ f ddF-x))

      (gfx:line-vertical (- center-x delta-y) (- center-y delta-x) (+ center-y delta-x) red green blue)
      (gfx:line-vertical (- center-x delta-x) (- center-y delta-y) (+ center-y delta-y) red green blue)
      (gfx:line-vertical (+ center-x delta-x) (- center-y delta-y) (+ center-y delta-y) red green blue)
      (gfx:line-vertical (+ center-x delta-y) (- center-y delta-x) (+ center-y delta-x) red green blue)

      )

    )
  )

; Example (gfx:circle-filled 75 50 30 0 0 255)

; end of circle-filled.lisp file

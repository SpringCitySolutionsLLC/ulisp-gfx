; circle.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a circle

; Midpoint Circle Algorithm as seen at
; https://en.wikipedia.org/wiki/Midpoint_circle_algorithm

; An example of the algorithm can be seen in C++ in the Adafruit GFX library
; https://github.com/adafruit/Adafruit-GFX-Library/blob/master/Adafruit_GFX.cpp

; The way the ddF additions work the same way as a difference engine calculates squares.

(defun gfx:circle (center-x center-y radius red green blue)
  (let ((f (- 1 radius))
        (ddF-x 1)
        (ddF-y (* -2 radius))
        (delta-x 0)
        (delta-y radius)
        )

    (display:pixel center-x (+ center-y radius) red green blue)
    (display:pixel center-x (- center-y radius) red green blue)
    (display:pixel (+ center-x radius) center-y red green blue)
    (display:pixel (- center-x radius) center-y red green blue)

    (loop

      (unless (< delta-x delta-y) (return))

      (cond ((>= f 0)
             (setq delta-y (- delta-y 1))
             (setq ddF-y (+ ddF-y 2))
             (setq f (+ f ddF-y)))
            )

      (setq delta-x (+ delta-x 1))
      (setq ddF-x (+ ddF-x 2))
      (setq f (+ f ddF-x))

      (display:pixel (+ center-x delta-x) (+ center-y delta-y) red green blue)
      (display:pixel (- center-x delta-x) (+ center-y delta-y) red green blue)
      (display:pixel (+ center-x delta-x) (- center-y delta-y) red green blue)
      (display:pixel (- center-x delta-x) (- center-y delta-y) red green blue)
      (display:pixel (+ center-x delta-y) (+ center-y delta-x) red green blue)
      (display:pixel (- center-x delta-y) (+ center-y delta-x) red green blue)
      (display:pixel (+ center-x delta-y) (- center-y delta-x) red green blue)
      (display:pixel (- center-x delta-y) (- center-y delta-x) red green blue)

      )

    )
  )

; Example (gfx:circle 75 50 30 255 0 255)

; end of circle.lisp file

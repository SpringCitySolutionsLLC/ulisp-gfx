; rectangle-filled.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a filled rectangle
; Assuming start is upper left corner and end is bottom right corner

(defun gfx:rectangle-filled (start-x start-y end-x end-y red green blue)
  (let ((delta-y 0))
    (setq delta-y (- end-y start-y))
    (dotimes (y delta-y)
      (gfx:line-horizontal start-x end-x (+ y start-y) red green blue))))

; end of rectangle-filled.lisp file

; line-low.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a line
; https://en.wikipedia.org/wiki/Bresenhams_line_algorithm#All_cases

(defun gfx:line-low (start-x start-y end-x end-y red green blue)
  (let ((delta-x (- end-x start-x))
        (delta-y (- end-y start-y))
        (inc-y 1)
        (difference 0)
        (current-y start-y))
    (when (< delta-y 0)
      (setq delta-y (- delta-y))
      (setq inc-y -1))
    (setq difference (- (* 2 delta-y) delta-x))
    (dotimes (x delta-x)
      (display:pixel (+ x start-x) current-y red green blue)
      (when (> difference 0)
        (setq current-y (+ current-y inc-y))
        (setq difference (- difference (* 2 delta-x))))
      (setq difference (+ difference (* 2 delta-y)))
      )
    )
  )

; end of line-low.lisp file

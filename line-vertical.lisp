; line-vertical.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a vertical line

(defun gfx:line-vertical (x start-y end-y red green blue)
  (let ((delta-y 0))
    (setq delta-y (- end-y start-y))
    (dotimes (y delta-y)
      (display:pixel x (+ y start-y) red green blue)
      )
    )
  )

; end of line-vertical.lisp file

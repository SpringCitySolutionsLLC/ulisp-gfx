# ulisp-gfx

A uLisp library of geometric graphics functions, generally inspired
by the concept of the Adafruit (tm) gfx library.

The screen driver merely has to plot pixels on a physical screen.
Given that, this library plots numerous pixels to generate
basic geometrical figures.

See the project wiki page at

https://gitlab.com/SpringCitySolutionsLLC/ulisp-gfx/wikis/home

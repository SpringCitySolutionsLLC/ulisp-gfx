; rectangle.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a rectangle
; Assuming start is upper left corner and end is bottom right corner

(defun gfx:rectangle (start-x start-y end-x end-y red green blue)
  (gfx:line-horizontal start-x end-x start-y red green blue)
  (gfx:line-vertical end-x start-y end-y red green blue)
  (gfx:line-horizontal start-x end-x end-y red green blue)
  (gfx:line-vertical start-x start-y end-y red green blue)
  )

; end of rectangle.lisp file

; triangle-filled.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a filled triangle

; The clearest plain English algorithm explanation I can find is at
; http://www.sunshine2k.de/coding/java/TriangleRasterization/TriangleRasterization.html

; At the other extreme of explaining the algo, an example is implemented in C++ at
; https://github.com/adafruit/Adafruit-GFX-Library/blob/master/Adafruit_GFX.cpp

; My attempt at explaining this specific algorithm
;
; Overall idea is to draw the triangle top to bottom, so sort the
; triangle points by height to find the top, middle, and bottom points.
;
; The fattest part of the triangle will always be
; the middle second height point,
; so one subtask is to draw horizontal lines between
; diverging sides from top to middle,
; the second subtask is to draw horizontal lines between converging sides
; from the second point height, to the bottom point.
;
; Optimize and skip the complicated algo for the cases of all
; three triangle points in a vert or horiz line.
;
; Handling flat top and flat bottom triangles is tricky because the line
; calculations will result in divide by zero errors, so offset
; the algo last row by one pixel to avoid those bad line calculations.
; The top subtask will never run in a flat top triangle
; and the top subtask will quit one line early for flat bottom triangles.

(defun gfx:triangle-filled (input-first-x
                             input-first-y
                             input-second-x
                             input-second-y
                             input-third-x
                             input-third-y
                             red green blue)

  (let ((first-x input-first-x)(first-y input-first-y)
        (second-x input-second-x)(second-y input-second-y)
        (third-x input-third-x)(third-y input-third-y))

    (when (> first-y second-y)
      (setq first-x input-second-x)
      (setq first-y input-second-y)
      (setq second-x input-first-x)
      (setq second-y input-first-y)
      (setq input-first-x first-x)
      (setq input-first-y first-y)
      (setq input-second-x second-x)
      (setq input-second-y second-y))

    (when (> second-y third-y)
      (setq second-x input-third-x)
      (setq second-y input-third-y)
      (setq third-x input-second-x)
      (setq third-y input-second-y)
      (setq input-second-x second-x)
      (setq input-second-y second-y))

    (when (> first-y second-y)
      (setq first-x input-second-x)
      (setq first-y input-second-y)
      (setq second-x input-first-x)
      (setq second-y input-first-y))

    (cond
      ((= first-y third-y)
       (gfx:line-horizontal (min first-x second-x third-x) (max first-x second-x third-x) first-y red green blue))

      ((= first-x second-x third-x)
       (gfx:line-vertical first-x first-y third-y red green blue))

      (t
        (let ((dx12 (- second-x first-x))
              (dy12 (- second-y first-y))
              (dx13 (- third-x first-x))
              (dy13 (- third-y first-y))
              (dx23 (- third-x second-x))
              (dy23 (- third-y second-y))
              (a 0)
              (b 0)
              (a-next 0)
              (b-next 0)
              (sa 0)
              (sb 0)
              (last 0))

          (if (= second-y third-y)
              (setq last second-y)
              (setq last (- second-y 1)))

          (dotimes (delta-y (- last first-y))

            (setq a-next (+ first-x (truncate (/ sa dy12))))
            (setq b-next (+ first-x (truncate (/ sb dy13))))
            (setq sa (+ sa dx12))
            (setq sb (+ sb dx13))

            (cond
              ((< a-next b-next)(setq a a-next)(setq b b-next))
              ((> a-next b-next)(setq a b-next)(setq b a-next))
              )

            (gfx:line-horizontal a (+ b 1) (+ first-y delta-y) red green blue)

            )

          (setq sa (* dx23 (- last second-y)))
          (setq sb (* dx13 (- last first-y)))

          (dotimes (delta-y (- third-y last))

            (setq a-next (+ second-x (truncate (/ sa dy23))))
            (setq b-next (+ first-x (truncate (/ sb dy13))))
            (setq sa (+ sa dx23))
            (setq sb (+ sb dx13))

            (cond
              ((< a-next b-next)(setq a a-next)(setq b b-next))
              ((> a-next b-next)(setq a b-next)(setq b a-next))
              )

            (gfx:line-horizontal a (+ b 1) (+ last delta-y) red green blue)

            )

          )
        )
      )
    )
  )

; Some testing cases to cut and paste
;
; Simple multicolor triangles in and out of y-axis order
; (gfx:triangle-filled 10 10 30 20 20 50 255 0 0)
; (gfx:triangle-filled 30 20 10 10 20 50 0 0 0)
; (gfx:triangle-filled 20 50 30 20 10 10 0 255 0)
; (gfx:triangle-filled 10 10 20 50 30 20 0 0 255)
;
; All blue points in a horiz line
; (gfx:triangle-filled 10 10 30 10 50 10 0 0 255)
;
; All green points in a vert line
; (gfx:triangle-filled 10 10 10 30 10 50 0 255 0)
;
; A flat top green triangle
; (gfx:triangle-filled 10 10 30 10 20 50 0 255 0)
;
; A flat bottom blue triangle
; (gfx:triangle-filled 10 10 20 50 30 50 0 0 255)
;

; end of triangle-filled.lisp file

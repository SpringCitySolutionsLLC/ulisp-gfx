; line-horizontal.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a horizontal line

(defun gfx:line-horizontal (start-x end-x y red green blue)
  (let ((delta-x 0))
    (setq delta-x (- end-x start-x))
    (dotimes (x delta-x)
      (display:pixel (+ x start-x) y red green blue)
      )
    )
  )

; end of line-horizontal.lisp file

; line-high.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a line
; https://en.wikipedia.org/wiki/Bresenhams_line_algorithm#All_cases

(defun gfx:line-high (start-x start-y end-x end-y red green blue)
  (let ((delta-x (- end-x start-x))
        (delta-y (- end-y start-y))
        (inc-x 1)
        (difference 0)
        (current-x start-x))
    (when (< delta-x 0)
      (setq delta-x (- delta-x))
      (setq inc-x -1))
    (setq difference (- (* 2 delta-x) delta-y))
    (dotimes (y delta-y)
      (display:pixel current-x (+ y start-y) red green blue)
      (when (> difference 0)
        (setq current-x (+ current-x inc-x))
        (setq difference (- difference (* 2 delta-y))))
      (setq difference (+ difference (* 2 delta-x)))
      )
    )
  )

; end of line-high.lisp file

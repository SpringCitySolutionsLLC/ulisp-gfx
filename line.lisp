; line.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a line
; This function selects the most optimized algorithm for each slope of line

(defun gfx:line (start-x start-y end-x end-y red green blue)
  (cond

    ((= start-y end-y) (if (> start-x end-x)
                           (gfx:line-horizontal end-x start-x start-y red green blue)
                           (gfx:line-horizontal start-x end-x start-y red green blue)
                           ))

    ((= start-x end-x) (if (> start-y end-y)
                           (gfx:line-vertical start-x end-y start-y red green blue)
                           (gfx:line-vertical start-x start-y end-y red green blue)
                           ))

    (t  (if (< (abs (- end-y start-y)) (abs (- end-x start-x)))
            (if (> start-x end-x)
                (gfx:line-low end-x end-y start-x start-y red green blue)
                (gfx:line-low start-x start-y end-x end-y red green blue)
                )
            (if (> start-y end-y)
                (gfx:line-high end-x end-y start-x start-y red green blue)
                (gfx:line-high start-x start-y end-x end-y red green blue)
                )
            )
       ))

  )

; end of line.lisp file

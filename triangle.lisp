; triangle.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Draw a triangle

(defun gfx:triangle (first-x first-y second-x second-y third-x third-y red green blue)
  (gfx:line first-x first-y second-x second-y red green blue)
  (gfx:line second-x second-y third-x third-y red green blue)
  (gfx:line third-x third-y first-x first-y red green blue)
  )

; end of triangle.lisp file

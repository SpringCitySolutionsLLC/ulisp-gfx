; clear-screen.lisp
; (C) 2019 Spring City Solutions LLC
; MIT License, see LICENSE file

; Clear the screen
; Its a convenience function.
; A 160*128 screen has 20480 pixels and
; it takes almost exactly one second to clear
; Thats about three hundred and forty pixels per second

(defun gfx:clear-screen (red green blue)
  (gfx:rectangle-filled 0 0
                   (+ 1 (eval *display:x-max*)) (+ 1 (eval *display:y-max*))
                   red green blue)
  )

; end of clear-screen.lisp file
